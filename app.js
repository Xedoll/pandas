//Librerie
var express = require('express');  
var fs  = require('fs');
var fileUpload = require('express-fileupload');
var core = require('./logger.js');        //funzioni di supporto per app.js
var send = require('./builder.js');     //funzioni costruzione pagine web

//Applicazione
var app = express();


//Path alle risorse
app.use('/css', express.static('views/css'));
app.use('/js', express.static('views/js'));
app.use('/img', express.static('views/img'));
app.use('/font', express.static('views/font'));

//Upload dei file
app.use(fileUpload());

//Path assoluto
app.use('/', express.static('/root'));

//Handle della richiesta per qualsisi URL
app.get('*', function(req, res){
    
    //logging della richiesta
    core.logReq(req);
    
    //Uri base per l'accesso alla cartella
    var uri = 'root' + decodeURI(req.url);
    
    //cerca il file
    fs.access(uri, 'r', function(err, fd) {
        
        //Se non esiste
        if(err){
            if(err.code === 'ENOENT'){
                core.logStatus(req.method, 404, 'file or directory '+uri+' does not exists!', res);
                send.statusPage(404, 'Sembra che la risorsa che stai cercando non esista!', res, req);
                return;
            }
        }
        
        //Esiste
        //Directory or file?
        fs.stat(uri, function(err, stats){
            
            //Errore file o directory non identificato
            if(err){
                core.logStatus(req.method, 404, 'file or directory '+uri+' does not exists!', res);
                send.statusPage(404, 'Sembra che la risorsa che stai cercando non esista!', res, req);
                return;
            }

            //Elabora come directory 
            if(stats.isDirectory())
            {
                core.logStatus(req.method, 200, 'Directory '+uri+' exists!', res);
                send.directoryPage(uri, res, req);
                return;
            }

            //Elabora come file
            if(stats.isFile()){
                //Errore file o directory non identificato
                core.logStatus(req.method, 200, 'File '+uri+' exists!', res);
                send.filePage(uri, res, req);
                return;
            } 
             
        }); 
    });
});

//Handle della richiesta POST
app.post('*', function(req, res){
    
    //Uri base per l'accesso alla cartella
    var uri = 'root' + decodeURI(req.url);
    
    fs.access(uri, 'w', function(err, fd) {
        
        //Se non esiste
        if(err){
            if(err.code === 'ENOENT'){
                core.logStatus(req.method, 404, 'file or directory '+uri+' does not exists!', res);
                send.statusPage(404, 'Sembra che la risorsa che stai cercando di usare non esista!', res, req);
                return;
            }
        }
        
        //Esiste
        //Directory or file?
        fs.stat(uri, function(err, stats){
            
            //Errore file o directory non identificato
            if(err){
                core.logStatus(req.method, 404, 'file or directory '+uri+' does not exists!', res);
                send.statusPage(404, 'Sembra che la risorsa che stai cercando non esista!', res, req);
                return;
            }
            
            //Elabora come directory 
            if(stats.isDirectory())
            {
                //Se i file non ci sono
                if (!req.files){
                    core.logStatus(req.method, 400, 'File not sent!', res);
                    send.statusPage(400, 'Sembra che tu non abbia inviato nessun file!', res, req);
                    return;
                }

                // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
                var file = req.files.sampleFile;

                // Use the mv() method to place the file somewhere on your server
                file.mv(uri + '/' + req.files.sampleFile.name, function(err) {
                    if (err){
                        core.logStatus(req.method, 500, 'File not uploaded!', res);
                        send.statusPage(500, 'Impossibile caricare il file specificato in '+uri+'!', res, req);
                        return;
                    }
                    send.directoryPage(uri, res, req);
                    core.logStatus(req.method, 200, 'File '+req.files.sampleFile.name+' uploaded!', res);
                });
            }
        }); 
    });  
});

//Inizializza e mette in ascolto il server
var server = app.listen(2604, function(req, res){
    
    //Variabili server
    var host = server.address().address;
    var port = server.address().port;
      
    //Output di verifica
    console.log('Server listening at http://%s:%s', host, port);
});


