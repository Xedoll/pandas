/*
    Filename    : builder.js
    Author      : Raffo Luca
    Created     : 17/05/2018 - 14:27
    Last update : 19/05/2018 - 03:23
    =============================
    
    Description : Module to build html page.
    =============================================
*/
var fs = require('fs');

module.exports = {
    
    /*
        Name        : statusPage
        Description : Send status page to client.
        Parameters  : 
            - status (HTTP Status code)
            - msg (message)
            - req (requests object)
            - res (response object)
        Return :
            - Nothing
    */
    statusPage : function(status, msg, res, req)
    {
        //Write the response HTTP head
        res.writeHead(status, {"Content-type" : "text/html"});
        
        //Write the page
        fs.readFile('views/header.html', function(err, file){
            if(err){
                res.write('header.html not found');
                return;
            }
            //Write header
            res.write(file);
            
            //Write the content
            res.write('<div class="container h-75"><div class="row h-100"><div class="col-sm-8 my-auto mx-auto"><div class="row"><div class="col-6 w-25 my-auto"><img src="/img/panda.svg" class="img-fluid" alt="Responsive image"></div><div class="col-6 w-100 my-auto mx-auto"><blockquote class="blockquote bq-warning"><p class="bq-title display-4">' + status + '</p><p class="h4 text-center">Mmmof! ' + msg + '</p></blockquote></div></div></div></div></div>');
            
            
            //Write the response footer
            fs.readFile('views/footer.html', function(err, file){
                if(err){
                    res.write('footer.html not found');
                    return;
                }
            
                res.write(file);
                
                //Send response
                res.end();
            });
        }); 
    },
    /*
        Name        : directoryPage
        Description : Send directory page to client.
        Parameters  : 
            - URI (unified resource identifier)
            - req (request object)
            - res (response object)
        return : 
            - nothing
    */
    directoryPage : function (uri, res, req)
    {
        //Write the response HTTP head
        res.writeHead(200, {"Content-type" : "text/html"});
        
        //Write the page
        fs.readFile('views/header.html', function(err, file){
            if(err){
                res.write('header.html not found');
                return;
            }
            //Write header
            res.write(file);
            
            //variabile riga
            var row = 0;
            
            //Write the content
            fs.readdir(uri, function(err, items) {
                
                var postUri;
                
                //Rimuovi root dalla base
                if(uri.startsWith('root')){
                    postUri = uri.slice(4,uri.length);
                }
                
                //Modal per l'input
                res.write('<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><form ref="uploadForm" id="uploadForm" action='+ postUri +' method="post" encType="multipart/form-data"><div class="modal-header"><h5 class="modal-title" id="uploadModalLabel">Upload</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><input type="file" name="sampleFile" /></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><input type="submit" class="btn btn-primary"></input></div></form></div></div></div>');
                
                for (var i=0; i<items.length; i++) {    
                    
                    //Gestisci eccezione formattazione URI
                    if(uri.endsWith('/')){
                        var file = uri + items[i];
                    }else{
                        var file = uri + "/"+ items[i];
                    }
                    
                    //Singolo file/directory
                    fs.stat(file, function(uri, filename){
                        return function(err, stats) {
                            
                            //Rimuovi root dalla base
                            if(uri.startsWith('root')){
                                uri = uri.slice(4,uri.length);
                            }
                    
                            //Gestisce in automatico le righe
                            if(row == 0){
                                res.write('<div class="row ml-0 mr-0 pl-0 pr-0">');
                            }
                            row = row + 1;
                            
                            
                            //Se è una directory
                            if(stats.isDirectory()){
                                
                                //Accorcia il nome della cartella se troppo lungo
                                if(filename.length >= 20){
                                    filename = filename.slice(0, 16);
                                    filename = filename.concat("...");
                                }
                                
                                res.write('<a href="' + uri + '" class="col-sm-6 col-md-4 col-lg-2 mt-2 mb-2"><div class="view overlay z-depth-1-half"><div class="card" style="height: 30%;"><div class="card-body"><h6 class="card-title">'+filename+'</h6><img src="/img/files_icons/folder.svg" class="img" style="width: 20%; height: 20%;"><p class="card-text"><ul class="list-unstyled"></ul></p></div></div><div class="mask flex-center rgba-yellow-light"></div></div></a>');
                                
                            }

                            //Se è un file
                            if(stats.isFile()){
                                
                                //Estrapola l'estensione
                                var filenameParts = filename.split('.');
                                var ext = filenameParts[filenameParts.length - 1];
                                
                                //Accorcia il nome del file se troppo lungo
                                if(filename.length >= 10){
                                    filename = filename.slice(0, 16);
                                    filename = filename.concat("...");
                                }
                                
                                //Elabora automaticamente il formato della dimensione del file
                                var size = stats["size"];
                                var sizeFormat = 'byte';
                                
                                if((size / 1000) > 1){
                                    size = size / 1000;
                                    sizeFormat = 'kb';
                                    if((size / 1000) > 1){
                                        size = size / 1000;
                                        sizeFormat = 'mb';
                                        if((size / 1000) > 1){
                                            size = size / 1000;
                                            sizeFormat = 'gb';
                                            if((size / 1000) > 1){
                                                size = size / 1000;
                                                sizeFormat = 'tb';
                                            }
                                        }
                                    } 
                                }
                                
                                //Arrotonda alla seconda cifra decimale
                                size = Math.round(size * 100) / 100;
                                
                                res.write('<a href="' + uri + '" class="col-sm-6 col-md-4 col-lg-2 mt-2 mb-2"><div class="view overlay z-depth-1-half mr-1 ml-1"><div class="card" style="height: 30%;"><div class="card-body"><h6 class="card-title">'+filename+'</h6><img src="/img/files_icons/'+ext+'.svg" class="img" style="width: 20%; height: 20%;"><p class="card-text"><ul class="list-unstyled"><li><small class="text-muted">Size : ' + size + ' ' + sizeFormat + ' </small></li><li><small class="text-muted">Extension : ' + ext + ' </small></li></ul></p></div></div><div class="mask flex-center rgba-blue-light"></div></div></a>');
                            }
                            
                            //Gestisce in automatico le righe
                            if(row == 6){
                                res.write('</div>');
                                row = 0;
                            }

                        }
                    }(file, items[i]));
                }
                
                //Write the response footer
                fs.readFile('views/footer.html', function(err, file){
                    if(err){
                        res.write('footer.html not found');
                        return;
                    }
                        
                    res.write('<a href="#" class="float" data-toggle="modal" data-target="#uploadModal"><i class="fa fa-plus my-float"></i></a>');
                    res.write(file);
                    
                    //Send response
                    res.end();
                });
            });
 
            
        }); 
    },
    /*
        Name        : filePage
        Description : Send file visualizzation page to client.
        Parameters  : 
            - file URI (unified resource identifier)
            - req (request object)
            - res (response object)
        return : 
            - nothing
    */
    filePage : function(uri, res, req)
    {
         //Read the content
        fs.readFile(uri, function(err, file) {

            if(err){
                res.write('file not found');
                res.end();
                return;
            }

            res.download(uri);
        });
    }
};