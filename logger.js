/*
    Filename    : logger.js
    Author      : Raffo Luca
    Created     : 17/05/2018 - 12:50
    Last update : /
    =============================
    
    Description : Funzioni di supporto per app.js
    =============================================
*/

module.exports = {
    
    logStatus : function(method, statusCode, message){
        console.log('%s status %i : %s', method, statusCode, message);
    },
    logReq : function(req){
        console.log('GET from IP => '+ req.ip +' HOSTNAME => '+req.hostname+' to URL =>'+req.url);
    }
    
};